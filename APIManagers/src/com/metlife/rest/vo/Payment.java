package com.metlife.rest.vo;

import java.io.Serializable;

public class Payment implements Serializable {


	private String pay_status=null;
	
	private String from_date=null;
	private String to_date=null;
	
	private String paid_date=null;

	

	public String getPay_status() {
		return pay_status;
	}

	public void setPay_status(String pay_status) {
		this.pay_status = pay_status;
	}

	public String getFrom_date() {
		return from_date;
	}

	public void setFrom_date(String from_date) {
		this.from_date = from_date;
	}

	public String getTo_date() {
		return to_date;
	}

	public void setTo_date(String to_date) {
		this.to_date = to_date;
	}

	public String getPaid_date() {
		return paid_date;
	}

	public void setPaid_date(String paid_date) {
		this.paid_date = paid_date;
	}

	

	public Payment(String pay_status, String from_date, String to_date, String paid_date) {
		super();
		this.pay_status = pay_status;
		this.from_date = from_date;
		this.to_date = to_date;
		this.paid_date = paid_date;
	}

	
}
