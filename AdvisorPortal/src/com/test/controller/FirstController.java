package com.test.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class FirstController{
 
   @RequestMapping(value="hello", method = RequestMethod.GET)
   public String printHello(ModelMap model) {
      model.addAttribute("message", "Hello Spring MVC Framework!");
      return "hello";
   }
   
   @RequestMapping(value="/claimTracker", method = RequestMethod.GET)
   public String cliamTracker(ModelMap model) {
      model.addAttribute("message", "Hello Spring MVC Framework!");
      return "claimTracker";
   }
   
   @RequestMapping(value="/premCalc", method = RequestMethod.GET)
   public String premiumCalculator(ModelMap model) {
      model.addAttribute("message", "Hello Spring MVC Framework!");
      return "premium_calculator";
   }
   
   @RequestMapping(value="/indvQuote", method = RequestMethod.GET)
   public String rwdIndvQuote(ModelMap model) {
      model.addAttribute("message", "Hello Spring MVC Framework!");
      return "rwdIndvQuote";
   }

}