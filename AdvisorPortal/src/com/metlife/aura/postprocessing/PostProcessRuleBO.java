package com.metlife.aura.postprocessing;

import org.drools.KnowledgeBase;

public class PostProcessRuleBO {
	
	private String originalDecision = null;
	private int totalLoading = 0;
	private int totalExlusion = 0;	
	private int totalMExlusion = 0;
	private int totalLExlusion = 0;
	
	private int originalLoading = 0;
	private int bmiLoading = 0;

	public KnowledgeBase kbase = null;
	public String postAURADecision=null;
	public String RuleFileName = null;	
	public String lob = null;
	public String brand = null;
	public String product = null;
	public String auraPostProcessID = null;
	public Double postAURALoading = 0.0;
	public String reason = null;
	public Boolean clientMatch = false;
	
	public Boolean existingUnit = Boolean.FALSE;
	public Boolean additionalUnit = Boolean.FALSE;
	
	public Boolean getAdditionalUnit() {
		return additionalUnit;
	}
	public void setAdditionalUnit(Boolean additionalUnit) {
		this.additionalUnit = additionalUnit;
	}
	public Boolean getExistingUnit() {
		return existingUnit;
	}
	public void setExistingUnit(Boolean existingUnit) {
		this.existingUnit = existingUnit;
	}
	public String getOriginalDecision() {
		return originalDecision;
	}
	public void setOriginalDecision(String originalDecision) {
		this.originalDecision = originalDecision;
	}
	public String getPostAURADecision() {
		return postAURADecision;
	}
	public void setPostAURADecision(String postAURADecision) {
		this.postAURADecision = postAURADecision;
	}
	
	public String getRuleFileName() {
		return RuleFileName;
	}
	public void setRuleFileName(String ruleFileName) {
		RuleFileName = ruleFileName;
	}
	public String getAuraPostProcessID() {
		return auraPostProcessID;
	}
	public void setAuraPostProcessID(String auraPostProcessID) {
		this.auraPostProcessID = auraPostProcessID;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getLob() {
		return lob;
	}
	public void setLob(String lob) {
		this.lob = lob;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	public int getTotalLoading() {
		return totalLoading;
	}
	public void setTotalLoading(int totalLoading) {
		this.totalLoading = totalLoading;
	}
	public int getTotalExlusion() {
		return totalExlusion;
	}
	public void setTotalExlusion(int totalExlusion) {
		this.totalExlusion = totalExlusion;
	}

	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Double getPostAURALoading() {
		return postAURALoading;
	}
	public void setPostAURALoading(Double postAURALoading) {
		this.postAURALoading = postAURALoading;
	}
	public KnowledgeBase getKbase() {
		return kbase;
	}
	public void setKbase(KnowledgeBase kbase) {
		this.kbase = kbase;
	}
	public int getTotalLExlusion() {
		return totalLExlusion;
	}
	public void setTotalLExlusion(int totalLExlusion) {
		this.totalLExlusion = totalLExlusion;
	}
	public int getTotalMExlusion() {
		return totalMExlusion;
	}
	public void setTotalMExlusion(int totalMExlusion) {
		this.totalMExlusion = totalMExlusion;
	}
	public int getBmiLoading() {
		return bmiLoading;
	}
	public void setBmiLoading(int bmiLoading) {
		this.bmiLoading = bmiLoading;
	}
	public int getOriginalLoading() {
		return originalLoading;
	}
	public void setOriginalLoading(int originalLoading) {
		this.originalLoading = originalLoading;
	}
	public Boolean getClientMatch() {
		return clientMatch;
	}
	public void setClientMatch(Boolean clientMatch) {
		this.clientMatch = clientMatch;
	}

}
