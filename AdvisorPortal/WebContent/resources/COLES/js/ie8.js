

$(document).ready(function () {
    
    if (!(/Android|Mobile|Tablet|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))) {
        CheckBrowser();
    }

    $('.browserUpgrade .close').click(function () {
        $('.browserUpgrade').hide();
    });
});

function CheckBrowser() {

    var cookie = document.cookie.split(';');
    var locInfo = null;
    for (var i = 0; i < cookie.length; i++) {
        var ul = $.trim(cookie[i]);
        if (ul.indexOf(cookiename) == 0) {
            locInfo = ul.substring(cookiename.length + 1, ul.length);
        }
    }
    
    if (locInfo == null || locInfo == "no") {

        var BrowserDetect = {
            init: function () {
                this.browser = this.searchString(this.dataBrowser) || "Other";
                this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
            },
            searchString: function (data) {
                for (var i = 0; i < data.length; i++) {
                    var dataString = data[i].string;
                    this.versionSearchString = data[i].subString;

                    if (dataString.indexOf(data[i].subString) !== -1) {
                        return data[i].identity;
                    }
                }
            },
            searchVersion: function (dataString) {
                var index = dataString.indexOf(this.versionSearchString);
                if (index === -1) {
                    return;
                }

                var rv = dataString.indexOf("rv:");
                var safrv = dataString.indexOf("Version/");
                if (this.versionSearchString === "Trident" && rv !== -1) {
                    return parseFloat(dataString.substring(rv + 3));
                }
                else if (this.versionSearchString === "Safari") {
                    if (safrv !== -1) {
                        return parseFloat(dataString.substring(safrv + 8));
                    }
                    else {
                        var version = parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
                        if (version <= 419.3)
                            return 2;
                        else return version;
                    }
                }
                else {
                    return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
                }
            },

            dataBrowser: [
                { string: navigator.userAgent, subString: "Chrome", identity: "Chrome" },
                { string: navigator.userAgent, subString: "MSIE", identity: "Explorer" },
                { string: navigator.userAgent, subString: "Trident", identity: "Explorer" },
                { string: navigator.userAgent, subString: "Firefox", identity: "Firefox" },
                { string: navigator.userAgent, subString: "Safari", identity: "Safari" }
            ]
        };
        BrowserDetect.init();
        
        if (BrowserDetect.browser == 'Explorer' && BrowserDetect.version <= expIE) {
                $('.browserUpgrade').show();
                $('.update-url').attr('href', urlIE);
                $('.browserUpgrade').attr('display', 'block');
                $('.browserUpgrade').attr('style', 'display:block');
            }
            else if (BrowserDetect.browser == 'Chrome' && BrowserDetect.version <= expChrome) {
                $('.browserUpgrade').show();
                $('.update-url').attr('href', urlChrome);
            }
            else if (BrowserDetect.browser == 'Firefox' && BrowserDetect.version <= expFirefox) {
                $('.browserUpgrade').show();
                $('.update-url').attr('href', urlFirefox);
            }
            else if (BrowserDetect.browser == 'Safari' && BrowserDetect.version <= expSafari) {
                $('.browserUpgrade').show();
                $('.update-url').attr('href', urlSafari);
            }
        else {
            $('.browserUpgrade').hide();
        }
    }
    else if (locInfo == "yes") {
        $('.browserUpgrade').hide();
    }
}

function UpdateCookie(disable) {
    var expiry = cookieExpiry;
    var date = new Date();
    //date.setDate(date.getDate() + expiry);
    date.setMinutes(date.getMinutes() + expiry * 60);
    var newCookie = [cookiename, '=', disable, '; host=', window.location.host.toString(), '; path=/;', 'expires=' + date.toUTCString() + ';'].join('');
    document.cookie = newCookie;

}

function Dismiss() {
    UpdateCookie('yes');
    $('.browserUpgrade').hide();
};