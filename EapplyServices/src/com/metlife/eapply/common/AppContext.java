/**
 * 
 */
package com.metlife.eapply.common;

import org.springframework.context.ApplicationContext;


public class AppContext {

	private static ApplicationContext applicationContext;

	/**
	 * @return the applicationContext
	 */
	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * @param applicationContext the applicationContext to set
	 */
	public static void setApplicationContext(ApplicationContext applicationContext) {
		AppContext.applicationContext = applicationContext;
	}  
	
	
	
}
