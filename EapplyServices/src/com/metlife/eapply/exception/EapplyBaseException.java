/**
 * 
 */
package com.metlife.eapply.exception;


public class EapplyBaseException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3708530425789385535L;

	public EapplyBaseException() {
		super();
	}

	public EapplyBaseException(final String message) {
		super(message);
	}

	public EapplyBaseException(final Throwable cause) {
		super(cause);
	}

	public EapplyBaseException(final String message, final Throwable cause) {
		super(message, cause);
	}

}
